def paises_total(paises, paises_inf):

    # crear lista con todos los paises sin repetir
    for i in range(len(datos[3])):
        if datos[3][i] in paises:
            pass
        else:
            paises.append(datos[3][i])
    # ordenar lista por nombre
    paises.sort()

    # crear lista con todos los paises sin repetir con infectados
    for i in range(len(datos[5])):
        if datos[5][i] != "0.0":
            if datos[3][i] in paises_inf:
                pass
            else:
                paises_inf.append(datos[3][i])
    # ordenar lista por nombre
    paises_inf.sort()
    return paises, paises_inf


def cal_t(infectados, muertos, recuperados, paises):
    # for que recorre los paises posibles
    for i in range(len(paises)):
        # agregar valores "0" para la suma de cada pais sin tener errores
        infectados.append(0)
        muertos.append(0)
        recuperados.append(0)
        # for que recorre y suma las 3 listas segun el pais
        for j in range(1, len(datos[5])):
            if datos[3][j] == paises[i]:
                # infectados[i] = infectados[i] + float(datos[5][j])
                muertos[i] = muertos[i] + float(datos[6][j])
                recuperados[i] = recuperados[i] + float(datos[7][j])

    return infectados, muertos, recuperados


def carga_datos():
    carga = []
    datos = open("txto.csv")
    for x in datos:
        carga.append(x)
    return carga


def agrupar(carga):
    datos = []
    # agrupar todos los datos en una lsita sobre lista
    for i in range(8):
        datos.append([])
        for j in range(len(carga)):
            x = carga[j].split(",")
            datos[i].append(x[i])
    return datos

# def rellenar(carga, datos):


if __name__ == "__main__":
    print("0.-mostrar los países con a lo menos un contagiado")
    print("1.-Listar de cada País el total de infectados confirmados.")
    print("2.-cuantos recuperados y cuantos muertos por país existen")

    # llamar funciones para cargar y agrupar datos
    carga = carga_datos()
    datos = agrupar(carga)
    buscar = int(input("Ingrese lo que desea buscar: "))
    # listas utilizadas (numeros de arriba segun la posicion de la matriz)
    paises = []
    # 3
    paises_inf = []
    # 5
    infectados = []
    # 6
    muertos = []
    # 7
    recuperados = []

    paises, paises_inf = paises_total(paises, paises_inf)
    infectados, muertos, recuperados = cal_t(infectados,
                                             muertos, recuperados, paises)

    # printear datos peddos
    if buscar == 0:
        print("Paises con almenos un infectado")
        for i in range(len(paises_inf)):
            print(paises_inf[i])
            print("\n")

    elif buscar == 1:
        for i in range(len(infectados)):
            print("Pais: ", paises_inf[i])
            print("Cantidad de infectados", infectados[i])
            print("\n")

    elif buscar == 2:
        for i in range(len(paises)):
            print("Pais:", paises[i])
            print("Cantidad de muertos:", muertos[i])
            print("Cantidad de recuperados", recuperados[i])
            print("\n")
    else:
        print("Error de opcion")
        print("Adios")

    # errores de carga largo
    """
    print(len(carga[555].split(",")))
    print(carga[555].split(","))
    print(len(carga[3].split(",")))
    print(carga[3].split(","))
    """
    """
    # comentado en caso de erro ry verificar, marcadores de prueba
    for i in range(len(datos[5])):
        if datos[5][i] == '2/1/2020 19:43':
            print(datos[5][i], i)
            """
