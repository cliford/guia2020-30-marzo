import os
import json
from pathlib import Path


# imprimir diccionario
def imprimir_dicc(aminoacido):
    for key, val in aminoacido.items():
        print("Nombre del aminoacido: ", key)
        print("Formula molecular : ", val)
        print("\n")


def opciones(aminoacido):
    # limitante de while
    continuar = True

    while continuar is not False:
        # seleccion de opcion
        opcion = int(input("Ingrese su opcion: "))

        # opcion agregar aminoacidos
        if opcion == 1:
            print("Vamos a agregar un amino acido ")
            amino = (input("Digite el aminoacido: ").upper())
            formula = (input("Digite la formula molecular: ").upper())
            aminoacido[amino] = formula
            datos = json.dumps(aminoacido)

            # abrir archivo json en write y cerrarlo
            f = open('aminos.json', 'w')
            f.write(datos)
            f.close

        # printear dicc
        elif opcion == 2:
            print("Aminoacidos ingresados: ")
            imprimir_dicc(aminoacido)

        # printear aminoacidos posibles para cambiar
        elif opcion == 3:
            print("Aquí estan todos los aminoacidos ingresados: ")
            imprimir_dicc(aminoacido)

            # ingresar aminoacido a cambiar
            # y el cambio de formula
            amino = str(input("Ingrese el aminoaciodo a cambiar"))
            cambio = str(input("Ingrese la nueva formula"))

            # poner todo en mayusculas
            amino = amino.upper()
            cambio = cambio.upper()

            # realizar cambio
            aminoacido[amino] = cambio

        elif opcion == 4:
            imprimir_dicc(aminoacido)
            # seleccionar aminoacido a eleiminar
            amino = str(input("Ingrese el aminoaciodo a eliminar: "))
            amino = amino.upper()
            del aminoacido[amino]

        elif opcion == 5:
            # lsita de aminoacidos que se buscan
            aminos_buscados = []
            molecula = str(input("Ingrese una molecula que tenga un amino: "))
            molecula = molecula.upper()

            # recorrer las key y values
            for key, value in aminoacido.items():
                if molecula in value:
                    aminos_buscados.append(key)

            if len(aminos_buscados) == 0:
                print("No hay aminoacidos con esa molecula")
            else:
                print("Lista con aminoacidos segun la busqueda: ")
                print(aminos_buscados)
            # vaciar lista para que no se acumulen
            aminos_buscados = []
        # opcino salir
        elif opcion == 6:
            print("Sayōnara Onii-chan")
            continuar = False
        # error de ingreso
        else:
            print("Opcion no valida ingrese otra")


if __name__ == "__main__":
    # crear dicc
    aminoacido = {}
    os.system("clear")

    # buscar archivo en el directorio y en caso qu eno esté crear uno
    ubicacion = Path("aminos.json")
    if ubicacion.exists():
        archivo = open("aminos.json", "r")
        datos = archivo.read()
        archivo.close()
        aminoacido = json.loads(datos)
    else:
        print("no hay datos para cargar")

    # ingreso de opciones
    print("MOCHI MOCHI ONII-CHAN\n")
    print("1.- Agregar aminoacido")
    print("2.- Ver aminoacidos ingredos")
    print("3.- Editar aminoacido")
    print("4.- Eliminar aminoacido")
    print("5.- Buscar aminoacido")
    print("6.- Salir\n")

    opciones(aminoacido)
